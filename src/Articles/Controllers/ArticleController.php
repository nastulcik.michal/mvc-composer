<?php

namespace App\CoreModule\Articles\Controllers;

use App\CoreModule\Articles\Models\ArticleManager;
use App\CoreModule\System\Controllers\Controller;
use DependencyInjectionConstruct;

/**
 * Process request on article
 */
class ArticleController extends Controller
{
    public $article_manager;

    /**
     * Article controller instnce
     * @var Controller
     */
    protected $controller;

    function __construct(
        DependencyInjectionConstruct $DI_construct,
        ArticleManager $article_manager
    )
    {
        $this->DI_construct = $DI_construct;
        $this->article_manager = $article_manager;
    }

    /**
     * @Action
     * Load article or controller
     * @param array $parameters Array with params for nested controll
     * @return void render article or controller content
     */
    public function index(array $parameters) : void
    {
        // Get article by URL
        $this->article_manager->loadArticle($parameters[0]);

        // If article dont find, redirect to ErrorController
        if (!$this->article_manager->article) {
            $this->redirect('404');
        }
        
        $this->data['title'] = $this->article_manager->article['title'];
        $this->data['showTitle'] = $this->article_manager->article['show_title'];
        $this->data['content'] = $this->article_manager->article['content'];
        $this->data['description'] = $this->article_manager->article['description'];

        // Call nested controller
        if ($this->article_manager->article['controller']) {
            $fullName = 'App\\' . $this->article_manager->article['controller'] . 'Controller';
            $controller = $this->DI_construct->returnInstance($fullName);
            array_shift($parameters); // Delete article url from parameters
            $controller->callActionFromParams($parameters);
            $this->data['controller'] = $controller;

            if (isset($this->data['controller']->data['title'])) {
                $this->data['title'] = $this->data['controller']->data['title'];
            } 
            if (isset($this->data['controller']->data['description'])) {
                $this->data['description'] = $this->data['controller']->data['description'];
            }
        }
        // Call article
        else {
            $this->data['controller'] = null;
        }

        // Set template
        $this->view = 'index';
    }
}