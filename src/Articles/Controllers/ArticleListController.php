<?php

namespace App\CoreModule\Articles\Controllers;

use App\CoreModule\Articles\Models\ArticleManager;
use App\CoreModule\System\Controllers\Controller;
use App\CoreModule\User\Models\UserManager;

/**
 * Show article list
 */
class ArticleListController extends Controller
{
    public $articleManager;

    /**
     * Article controller instance
     * @var Controller
     */
    protected $controller;

    function __construct(
        UserManager $userManager,
        ArticleManager $articleManager
    ) {
        $this->userManager = $userManager;
        $this->articleManager = $articleManager;
    }

    /**
     * @Action
     * Load article list
     */
    public function index()
    {
        $this->authAdminUser();

        // Set variables for template
        $this->data['title'] = $this->articleManager->article['title'];

        // Show h1 on page or not
        $this->data['showTitle'] = $this->articleManager->article['show_title'];

        $this->data['content'] = $this->articleManager->article['content'];

        $this->data['articles'] = $this->articleManager->getArticles($this->userManager->user['super_admin']);

        // Set template
        $this->view = 'article-list';
    }
}
