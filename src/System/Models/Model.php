<?php 

namespace App\CoreModule\System\Models;

use TranslateHelper;
use DbHelper;

/**
 * Parent for all models
 */
class Model
{
	function __construct(DbHelper $db)
	{
		$this->db = $db;
	}
	
	public function translate($text)
    {
        return TranslateHelper::get($text);        
    }
}