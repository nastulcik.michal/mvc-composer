<?php 

namespace App\CoreModule\System\Models;

use DbHelper;

/**
 * Parent for classes Manager that work with database
 */
abstract class DbManager
{
	function __construct(DbHelper $db)
	{
		$this->db = $db;
	}

    /**
	 * Return database table name
	 * @return string Table name
	 */
	protected abstract function getDbTableName() : string;

    public function save(array $data): void
    {
        try {
            $data['_updated'] = date("Y-m-d H:i:s");
            $data['_created'] = date("Y-m-d H:i:s");
            $this->db->insert($this->getDbTableName(), $data);
        } catch (\PDOException $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function update(array $data): void
    {
        try {
            $data['_updated'] = date("Y-m-d H:i:s");
            $this->db->update($this->getDbTableName(), $data, "WHERE id = ?", [$data['id']]);
        }
        catch (\PDOException $ex)
        {
            throw new \Exception($ex->getMessage());
        }
    }

    public function getRecordById(int $id)
    {
        return $this->db->queryOne("SELECT * FROM " . $this->getDbTableName() . " WHERE  id = ?", [ $id ] );
    }

    public function delete(int $id): bool
    {
        $r = $this->db->delete($this->getDbTableName(), "WHERE id = ?", [ $id ] );
        return ($r == 1) ? true : false;
    }

    public function getOneRecordBy(string $colum_name, string $colum_value)
    {
        return $this->db->queryOne("SELECT * FROM " . $this->getDbTableName() . " WHERE " . $colum_name . " = ?", [ $colum_value ] );
    }

    public function getAll(string $order_by = 'id') : array
    {
        return $this->db->queryAll("SELECT * FROM " .  $this->getDbTableName() . " ORDER BY ?", [$order_by]);
    }
}