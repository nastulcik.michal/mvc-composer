<?php

namespace App\CoreModule\System\Controllers;

use App\CoreModule\Articles\Models\ArticleManager;
use App\CoreModule\User\Models\UserManager;
use Utils\Utility\StringUtils;
use Utils\UserException;
use ReflectionClass;
use Exception;
use ReflectionException;
use ReflectionMethod;
use Settings;

/**
 * Parent for all controllers in application
 */
abstract class Controller
{
    const MSG_INFO = 'info';
    const MSG_SUCCESS = 'success';
    const MSG_ERROR = 'danger';
    const MSG_WARNING = 'warning';

    /**
     * @var bool If controller create API no article
     */
    protected $createdByApi;

    /**
     * @var array Array with data -> For template translate indexes to variable
     */
    protected $data = array();
    /**
     * @var string Template name without suffix
     */
    protected $view = "";

    /**
     * Path to folder with template
     * @var string
     */
    protected $templatePath;

    /**
     * Control if user is login, if not redirect to "login"
     */
    public function authUser()
    {
        if (!isset($_SESSION['user'])) {
            $this->addMessage("For visit this page you must be login");
            $this->redirect('login');
        }
    }

    /**
     * Control if user is admin
     */
    public function authAdminUser()
    {
        if (!isset($_SESSION['user']) || $_SESSION['user']['admin'] != 1) {
            $this->addMessage("For visit this page you must be admin");
            $this->redirect('login');
        }
    }

    public function getUserId()
    {
        if (isset($_SESSION['user']['id'])) {
            return $_SESSION['user']['id'];
        }
    }

    public function userLogin()
    {
        if (!isset($_SESSION['user'])) {
            return false;
        }
        return true;
    }

    public function logOutUser()
    {
        if (isset($_SESSION['user'])) {
            unset($_SESSION['user']);
            $this->addMessage("You are successfully log out");
            $this->redirect('home');
        }
    }

    /**
     * Render view
     */
    public function renderView()
    {
        if ($this->view) {
            // TODO: make it better
            // extract($this->protect($this->data)); 
            extract($this->data);
            extract($this->data, EXTR_PREFIX_ALL, "");
            $reflect = new ReflectionClass(get_class($this));
            $path = str_replace('Controllers', 'Views', str_replace('\\', '/', $reflect->getNamespaceName()));
            $controllerName = str_replace('Controller', '', $reflect->getShortName());
            require($this->getPathToContoller($path, $controllerName));
        }
    }

    public function getPathToContoller($path, $controllerName)
    {
        $e = explode("/", $path);
        
        // Create path to controller
        if ($e[1] != "CoreModule") {
            return '../a' . ltrim($path, 'A') . '/' . $controllerName . '/' . $this->view . '.phtml';
        }

        if ($this->view == "layout_backend" || $this->view == "layout_frontend") {
            return ($this->view == "layout_backend") ? Settings::$layout_template_paths['backend'] : Settings::$layout_template_paths['frontend'];
        }
        return '../vendor/mvc/core/src/' . $e[2] ."/". $e[3]. "/" . $controllerName . '/' . $this->view . '.phtml';
    }

    /**
     * Return path to template by actual controller
     * @return strong Path to template folder
     */
    public function getTemplatePath()
    {
        if ($this->templatePath) {
            return $this->templatePath;
        }

        $reflect = new ReflectionClass(get_class($this));
        $path = str_replace('Controllers', 'Views', str_replace('\\', '/', $reflect->getNamespaceName()));
        $controllerName = str_replace('Controller', '', $reflect->getShortName());
        $e = explode("/", $path);

        if ($e[1] == "CoreModule") {
            $this->templatePath = '../vendor/mvc/core/src/' . $e[2] . "/" . $e[3] . "/" . $controllerName . '/';
        } else {
            $this->templatePath = '../a' . ltrim($path, 'A') . '/' . $controllerName . '/';
        }

        return $this->templatePath;
    }

    /**
     * Load template from actual controller folder
     * example -> Project/Views/ControllerName/templateName.phtml
     * @param   $name     TemplateName
     */
    public function loadTemplate($name, $data = [])
    {
        // Load data for template
        extract($this->protect($data));
        extract($data, EXTR_PREFIX_ALL, "");

        require($this->getTemplatePath() . $name .  ".phtml");
    }

    /**
     * Load view template -> classic load like template but with delving by view
     * example -> Project/Views/ControllerName/viewName/templateName.phtml
     * @param $name TemplateName
     */
    public function loadViewTemplate($name, $data = [])
    {
        // Load data for template
        extract($this->protect($data));
        extract($data, EXTR_PREFIX_ALL, "");

        require($this->getTemplatePath() . $this->view. "/" . $name .  ".phtml");
    }

    /**
     * Load block template - block template is for template which can be use across the whole web
     * Path for block template is set in config
     * @param $name BlockName
     */
    public function loadBlockTemplate($name, $data = [])
    {
        // Load data for template
        extract($this->protect($data));
        extract($data, EXTR_PREFIX_ALL, "");
        require(Settings::$layout_template_paths['template_blocks'] . $name .  ".phtml");
    }

    /**
     * Call required action on controller by parameters
     * @param  array  $params Params
     * @param  boolean $mode  Actual mode - Action,ApiAction,CliAction
     * @return void
     */
    public function callActionFromParams($params, $mode = "Action")
    {
        // Getting the called action
        $action = StringUtils::hyphensToCamel($params ? array_shift($params) : 'index');

        // Getting information about method
        try {
            $method = new ReflectionMethod(get_class($this), $action);
        } catch (ReflectionException $exception) {
            throw new Exception("Neznámá akce - $action");
        }

        // Control access
        $phpDoc = $method->getDocComment();
        $annotation = '@' . $mode;

        if (mb_strpos($phpDoc, $annotation) === false) {
            throw new Exception("Neplatná akce - $action");
        }

        $requiredParamsCount = $method->getNumberOfRequiredParameters();

        // Controll params for calling action
        if (count($params) < $requiredParamsCount) {
            $this->throwRoutingException("Akci nebyly předány potřebné parametry ($requiredParamsCount)");
        }

        // Insert arguments into calling action
        $method->invokeArgs($this, $params);
    }

    /**
     * Add message for user
     * @param string $content Content message
     * @param string $type    Message type
     */
    public function addMessage($content, $type = self::MSG_INFO)
    {
        $message = array(
            'content' => $content,
            'type' => $type,
        );
        if (isset($_SESSION['messages']))
            $_SESSION['messages'][] = $message;
        else
            $_SESSION['messages'] = array($message);
    }

    /**
     * Return messages for user
     * @return array Messages
     */
    public function getMessages()
    {
        if (isset($_SESSION['messages'])) {
            $messages = $_SESSION['messages'];
            unset($_SESSION['messages']);
            return $messages;
        } else {
            return array();
        }
    }

    /**
     * Redirect on url
     * @param string $url URL
     */
    public function redirect($url = '')
    {
        if (!$url) {
            $url = ArticleManager::$article['url'];
        }

        // Redirect on homePage
        if ($url == "/") {
            header("Location: /");
            header("Connection: close");
            exit;
        }
        header("Location: /$url");
        header("Connection: close");
        exit;
    }

    /**
     * In debug mode throws an exception, otherwise redirect on 404
     * @param string $message Debug message
     * @throws Exception
     */
    private function throwRoutingException($message)
    {
        if (Settings::$debug)
            throw new Exception($message);
        else
            $this->redirect('chyba');
    }

    /**
     * Treate variable for extract into HTML page
     * @param mixed $x Proměnná k ošetření
     * @return mixed Ošetřená proměnná
     */
    private function protect($x = null)
    {
        if (!isset($x))
            return null;
        elseif (is_string($x))
            return htmlspecialchars($x, ENT_QUOTES);
        elseif (is_array($x)) {
            foreach ($x as $k => $v) {
                $x[$k] = $this->protect($v);
            }
            return $x;
        } else
            return $x;
    }
}
