<?php

namespace App\CoreModule\System\Controllers;

use App\CoreModule\Articles\Controllers\ArticleController;
use App\CoreModule\Articles\Models\ArticleManager;
use DependencyInjectionConstruct;
use Settings;

class RouterController extends Controller
{
    /**
     * @var ArticleController
     */
    protected $articleController;

    /**
     * Instance dependenci injection container
     * @var DicConstruct
     */
    protected $dependInjectConst;

    /**
     * Instance article manager
     * @var ArticleManager
     */
    protected $articleManager;

    /**
     * Uloží instanci kontejneru pro dependenci injection do proměné
     */
    public function setDicContainer($dependInjectConst)
    {
        $this->dependInjectConst = $dependInjectConst;
    }

    function __construct(
        DependencyInjectionConstruct $DIconstruct,
        ArticleController $articleController,
        ArticleManager $articleManager
    )
    {
        $this->DIconstruct = $DIconstruct;
        $this->articleController = $articleController;
        $this->articleManager = $articleManager;
    }

    /**
     * Parsing url by slashes and return array contain params
     * @param string $url URL address
     * @return array Parsed url address
     */
    private function parseUrl($url)
    {
        $parsedUrl = parse_url($url);
        
        // Delete first slash
        $parsedUrl["path"] = ltrim($parsedUrl["path"], "/");
        
        // Delete empty space around url path
        $parsedUrl["path"] = trim($parsedUrl["path"]);
        
        // Explode by slashes
        $splitPath = explode("/", $parsedUrl["path"]);
        return $splitPath;
    }

    /**
     * Parse url and call controller
     * @param array   $parameters Under index 0 is address on parse
     * @param boolean $cli        Send request from cli
     * @return void
     */
    public function index($parameters, $cli = false)
    {
        $parsedUrl = $this->parseUrl($parameters[0]);

        if (empty($parsedUrl[0])){
            $parsedUrl[0] = 'home';
        }
            
        // Proccess request for API
        if ($parsedUrl[0] == "api")
        {
            array_shift($parsedUrl); // delete first param "api"
            $this->processRequest($parsedUrl, "ApiAction");
        }
        // Proccess request for AJAX
        elseif ($parsedUrl[0] == "ajax")
        {
            array_shift($parsedUrl); // delete first param "ajax"
            $this->processRequest($parsedUrl, "AjaxAction");
        }
        elseif ($cli)
        {
            $this->processRequest($parsedUrl, "CliAction");
        }
        else{
            $this->processArticleRequest($parsedUrl);
        }
    }

    /**
     * Call appropriate controller with action by params 
     * @param  array $parameters Parsed url
     * @param  string $mode      Process mode - ApiAction/AjaxAction/CliAction
     * @return void
     */
    public function processRequest($parameters, $mode)
    {
        // Explode namespace by "-" and add "Controllers" into array
        $pieces = explode("-", array_shift($parameters));
        array_splice($pieces, count($pieces) - 1, 0, 'Controllers');
        $controllerPath = implode("\\", $pieces);
        $controllerPath .= "Controller";

        if (preg_match('/^[a-zA-Z0-9\\\\]*$/u', $controllerPath)) {
            // Call controller
            $controller = $this->DIconstruct->returnInstance($controllerPath);
            $controller->callActionFromParams($parameters, $mode);
        } else {
            $this->redirect('error');
        }
    }

    /**
     * Call article instance which call article or appropriete controller by data in db
     * @param  array $parameters Parsed url
     * @return void            
     */
    public function processArticleRequest($parameters)
    {
        // Call controller
        $this->articleController->index($parameters);

        // Set variables for template
        $this->data['title'] = $this->articleController->data['title'];
        $this->data['description'] = $this->articleController->data['description'];
        $this->data['keywords'] = $this->articleManager->article['keywords'];
        
        // Whether page will have fullpage loyout or not
        $this->data['fullWidth'] = $this->articleManager->article['full_width'];
        $this->data['messages'] = $this->getMessages();
        $this->data['domain'] = Settings::$domain;
        $this->data['url'] = $this->articleManager->article['url'];
        
        // Set template
        $this->view = ($this->articleManager->article['backend_page']) ? 'layout_backend' : 'layout_frontend';
    }
}