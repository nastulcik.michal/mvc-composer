<?php

/**
 * Wrapper for translate text in system
 */
class TranslateHelper {

	/**
	 * @var Translate template
	 */
	public static $library;

	/**
	 * @param string $langCode Language code for translate 
	*/
	public $transLang = 'default';

	public $enableLang = ['sk','eng','pl','hun'];

	public function init($db)
	{
		if (isset($_SESSION['lang']) && in_array($_SESSION['lang'], $this->enableLang)) {
			$this->transLang = $_SESSION['lang'];
		}
		$this->prepareData($db);
	}

	/**
	 * Prepare data for translate text in application
	 */
	private function prepareData($db) {
		if (!self::$library) {
			$res = $db->queryAll('SELECT `default`,`'.$this->transLang.'` FROM trans_library');
			foreach ($res as $key => $val) {
				self::$library[$val['default']] = $val[$this->transLang];
			}
		}
	}

	public static function get($text)
	{
		if (isset(self::$library[$text])) {
			return self::$library[$text];
		}
		return $text;
	}
	
	/**
	 * Create annonymous class for use in template view
	 */
	public static function getLibraryClass()
	{
		return new class("trans") extends TranslateHelper {
		};
	}
}
