<?php

namespace App\CoreModule\User\Models;

use Utils\UserException;
use PDOException;
use DbHelper;

/**
 * Class for manage user
 */
class UserManager
{
    /**
     * User data
     * @var array|false
     */
    public $user;

    /**
     * User id
     * @var int
     */
    public $userId;

    /**
     * Database wrapper
     * @var DbHelper
     */
    public $db;

    function __construct(DbHelper $db)
    {
        $this->db = $db;
        $this->user = $this->loadUser();
        $this->userId = ($this->user) ? $this->user['id'] : false;
    }

    public function loadUser()
    {
        if (isset($_SESSION['user'])) {
            return $_SESSION['user'];
        }
        return false;
    }

    /**
     * Control and save user
     * @param array Registration data
     */
    public function register($data)
    {
        if ($data) {
            try {
                $usurname = $this->db->query("SELECT * FROM `users` WHERE `username` = ?", [$data['username']]);
                $email = $this->db->query("SELECT * FROM `users` WHERE `email` = ?", [$data['email']]);

                if (!empty($username)) {
                    throw new UserException('Toto uživatelské jméno již bohužel existuje');
                }

               if (!empty($email)) {
                    throw new UserException('Uživatel s tímto emailem již bohužel existuje');
                }

                if ($data['password'] != $data["password_repeat"]) {
                    throw new UserException('Hesla se neshodují, zkuste to prosím znovu.');
                }

                // Delete password_repeat
                unset($data['password_repeat']);

                // Hashing password
                $data['password'] = $this->hashPassword($data['password']);

                 // Set times
                $data['created_at'] = date("Y-m-d H:i:s");
                $data['updated_at'] = date("Y-m-d H:i:s");

                // Aby se provedl autoincrement, hodnota musí být NULL, nebo sloupeček z dotazu musíme vynechat
                $this->db->insert('users', $data);
            }
            catch (PDOException $ex)
            {
                throw new UserException($ex->getMessage());
            }
        }
    }

    /**
     * Login user to application
     * @param  string $login Username\Email
     * @param  string $password Password plain text
     * @return void
     */
    public function login($login, $password)
    {
        $user = $this->db->queryOne("SELECT * FROM `users` WHERE `username` = ? OR `email` = ?", [$login, $login]);
        
        if (!$user) {
            throw new UserException("Neexistující jméno/email");
        } 

        if (!password_verify($password, $user['password'])) {
          throw new UserException("Špatné heslo, zkuste to znovu");  
        }

        $this->user = $user;
        $this->deleteDefectiveParams();
        $_SESSION['user'] = $this->user;
    }

    /**
     * Update data for user
     * @param array Data pro registraci
     */
    public function update($data)
    {
        if ($data) {
            try {
                $data['updated_at'] = date("Y-m-d H:i:s");
                $this->db->update('users', $data, " WHERE id = ?", [$this->user['id']]);
                // Load new user data
                $this->reloadUser($this->user['id']);
            }
            catch (PDOException $ex)
            {
                throw new UserException($ex->getMessage());
            }
        }
    }

    public function deleteUser()
    {
        $this->db->delete('users', "WHERE id = ?", [$this->user['id']]);
    }

    public function deleteUserById($id)
    {
        $this->db->delete('users', "WHERE id = ?", [$id]);
    }

    /**
     * Get all users from table
     * @param  integer $limit Return user limit
     * @return array          Users array
     */
    public function getUsers()
    {
        return $this->db->queryAll("SELECT * FROM `users`");
    }

    public function reloadUser($userId)
    {
        $this->user = $this->db->queryOne("SELECT * FROM `users` WHERE `id` = ?", [$userId]);
        $this->deleteDefectiveParams();
        $_SESSION['user'] = $this->user;
    }

    public function deleteDefectiveParams()
    {
        unset($this->user['password'], $this->user['password_reset_link'], $this->user['email_verification_link']);
    }

    // Control if exist user with identic reset link + update if exist
    public function controlVerificationEmailLink($link)
    {
        $user = $this->getUserBy('email_verification_link', $link);
        if (!$user) {
            return false;
        }

        $data = ['email_verification_link' => NULL, 'email_verification' => 1];
        $this->db->update('users', $data, " WHERE id = ?", [$user['id']]);
        $this->reloadUser($user['id']);
        return true;
    }

    public function passwordUpdate($data)
    {
        if ($data) {
            if ($data['new_password'] != $data['new_password_repeat']) {
                throw new UserException("Zadali jste špatně kontrolu hesla.");
            }

            $hashPassword = $this->db->querySingle("SELECT password FROM `users` WHERE `id` = ?", [$this->user['id']]);

            if (!$this->controlPassword($data['old_password'], $hashPassword)) {
                throw new UserException("Zadali jste špatné heslo.");
            }

            try {
                $newHashPassword['password'] = $this->hashPassword($data['new_password']);
                $this->db->update('users', $newHashPassword, " WHERE id = ?", [$this->user['id']]);
            }
            catch (PDOException $ex)
            {
                throw new UserException($ex->getMessage());
            }
        }
    }

    public function passwordReset($data)
    {
        if ($data) {
            if ($data['new_password'] != $data['new_password_repeat']) {
                throw new UserException("Zadali jste špatně potvrzení hesla.");
            }

            try {
                $hashPassword = $this->hashPassword($data['new_password']);
                $values = ["password" => $hashPassword, "password_reset_link" => ""];
                $this->db->update('users', $values, " WHERE id = ?", [$data['user_id']]);
            }
            catch (PDOException $ex)
            {
                throw new UserException($ex->getMessage());
            }
        }
    }

    public function generatePasswordResetLink($email)
    {
        $user = $this->getUserByEmail($email);
        $resetLink = $this->generateRandomString(60);
        $this->updateResetPasswordLink($resetLink, $user['id']);
        return $resetLink;
    }

    public function generateEmailVerificationLink()
    {
        $verifactionLink = $this->generateRandomString(60);
        $this->update(['email_verification_link' => $verifactionLink]);
        return $verifactionLink;
    }

    public function updateEmailVerification()
    {
        $this->update(['email_verification' => 1]);
    }

    public function updateResetPasswordLink($resetLink, $userId)
    {
        $this->db->update('users', ['password_reset_link' => $resetLink], " WHERE id = ?", [$userId]);
    }

    private function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function getUserBy($param, $value)
    {
        $res = $this->db->queryOne("SELECT * FROM `users` WHERE " . $param . " = ?", [$value]);
        if (!$res) 
            return false;
        return $res;
    }

    public function getUserByEmail($email)
    {
        $userByEmail = $this->getUserBy("email", $email);
        if (!$userByEmail) 
            throw new UserException("Zadaný email neexistuje");
        return $userByEmail;
    }

    public function hashPassword($password)
    {
        return password_hash($password, PASSWORD_DEFAULT);
    }

    public function controlPassword($password, $passwordHash)
    {
        return password_verify($password, $passwordHash) ? true : false;
    }

    public function getUserByResetLink($resetLink)
    {
        return $this->getUserBy('password_reset_link', $resetLink);
    }
}