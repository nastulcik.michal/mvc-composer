<?php

namespace App\CoreModule\User\Controllers;

use App\CoreModule\Articles\Models\ArticleManager;
use App\CoreModule\System\Controllers\Controller;
use App\CoreModule\User\Models\UserManager;
use Utils\UserException;
use Utils\HtmlBuilder;
use Utils\Forms\Form;
use Settings;

/**
 * Proccess request on user login
 */
class LoginController extends Controller
{
    /**
     * Manager for article
     * @var ArticleManager
     */
    public $articleManager;

    /**
     * Class for manage user
     * @var UserManager
     */
    public $userManager;

    function __construct(
        ArticleManager $articleManager,
        UserManager $userManager
    )
    {
        $this->articleManager = $articleManager;
        $this->userManager = $userManager;
    }

    /**
     * @Action
     * Login user
     */
    public function index()
    {
        // User is already login -> redirect to adminstration
        if ($this->userLogin()){
            $this->redirect('administration');
        }

        $form = $this->loginForm();
        $this->data['form'] = $form;

        if ($form->isPostBack())
        {
            try
            {
                $data = $form->getData();
                $this->userManager->login($data['login'], $data['password']);
                $this->redirect('administration');
            }
            catch (UserException $ex)
            {
                $this->addMessage($ex->getMessage());
            }
        }

        $this->data['title'] = $this->articleManager->article['title'];
        $this->data['content'] = $this->articleManager->article['content'];

        // Set template
        $this->view = 'index';
    }

    public function loginForm()
    {
        $form = new Form('login');
        $form->addTextBox('login', 'Email', true, ['class' => 'form-control-sm','placeholder' => 'pepik@email.cz']);
        $form->addPasswordBox('password', 'Heslo', true, ['class' => 'form-control-sm','placeholder' => '*******'])
                ->setTooltip("Minimální délka hesla je 6 znaků");
        if (Settings::$localhost === false) {
            $form->addRecaptchaBox(Settings::$recaptcha['secret_key'], Settings::$recaptcha['site_key'], 0.7);
        }
        $form->addButton('submit' , 'Přihlásit', ['class' => 'btn-sm btn-warning rounded-pill float-right mt-3']);

        return $form;
    }
}